/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.feedingthemachine.examen1.controller;

/**
 *
 * @author feedingthemachine
 */
public class CalcularInteres {
    
    public double getValues(int capital, int tasa, int años){
        double tasa_op = (double) tasa/100;
        double intereses = (capital*tasa_op*años);
        return intereses;
    }
    
}
